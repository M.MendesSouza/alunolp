object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Form5'
  ClientHeight = 261
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 48
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 32
    Top = 96
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 32
    Top = 144
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object DBEdit1: TDBEdit
    Left = 96
    Top = 45
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule4.DataSourcePokemon
    TabOrder = 0
  end
  object DBEdit3: TDBEdit
    Left = 96
    Top = 136
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule4.DataSourcePokemon
    TabOrder = 1
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 96
    Top = 88
    Width = 145
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule4.DataSourcePokemon
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule4.DataSourceTreinador
    TabOrder = 2
  end
  object ButtonCancelar: TButton
    Left = 48
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = ButtonCancelarClick
  end
  object ButtonSalvar: TButton
    Left = 166
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = ButtonSalvarClick
  end
end
