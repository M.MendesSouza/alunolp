object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 530
  ClientWidth = 769
  Color = clMaroon
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 392
    Top = 64
    Width = 329
    Height = 305
    Color = clSilver
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 32
      Top = 32
      Width = 61
      Height = 13
      Caption = 'Identificador'
    end
    object Label2: TLabel
      Left = 32
      Top = 83
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object Label3: TLabel
      Left = 32
      Top = 128
      Width = 43
      Height = 13
      Caption = 'Pokemon'
    end
    object Label4: TLabel
      Left = 32
      Top = 176
      Width = 23
      Height = 13
      Caption = 'Nivel'
    end
    object id: TDBEdit
      Left = 103
      Top = 29
      Width = 121
      Height = 21
      DataField = 'id'
      DataSource = DataModule3.DataSource1
      TabOrder = 0
    end
    object id_treinador: TDBEdit
      Left = 103
      Top = 80
      Width = 121
      Height = 21
      DataField = 'id_treinador'
      DataSource = DataModule3.DataSource1
      TabOrder = 1
    end
    object nome: TDBEdit
      Left = 103
      Top = 125
      Width = 121
      Height = 21
      DataField = 'nome'
      DataSource = DataModule3.DataSource1
      TabOrder = 2
    end
    object nivel: TDBEdit
      Left = 103
      Top = 173
      Width = 121
      Height = 21
      DataField = 'nivel'
      DataSource = DataModule3.DataSource1
      TabOrder = 3
    end
  end
  object Button1: TButton
    Left = 504
    Top = 399
    Width = 121
    Height = 25
    Caption = 'Editar Pokemon'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 504
    Top = 430
    Width = 121
    Height = 25
    Caption = 'Inserir Pokemon'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 504
    Top = 461
    Width = 121
    Height = 25
    Caption = 'Deletar Pokemon'
    TabOrder = 3
    OnClick = Button3Click
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 32
    Top = 64
    Width = 313
    Height = 420
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule3.DataSource1
    TabOrder = 4
  end
end
