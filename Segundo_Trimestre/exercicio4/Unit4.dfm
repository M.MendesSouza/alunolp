object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 367
  ClientWidth = 588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Identificador: TLabel
    Left = 240
    Top = 131
    Width = 61
    Height = 13
    Caption = 'Identificador'
  end
  object Nome: TLabel
    Left = 240
    Top = 158
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Nivel: TLabel
    Left = 240
    Top = 212
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object Label1: TLabel
    Left = 240
    Top = 193
    Width = 44
    Height = 13
    Caption = 'treinador'
  end
  object id: TDBEdit
    Left = 328
    Top = 128
    Width = 121
    Height = 21
    DataField = 'id'
    DataSource = DataModule3.DataSource1
    TabOrder = 0
  end
  object name: TDBEdit
    Left = 328
    Top = 155
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule3.DataSource1
    TabOrder = 1
  end
  object niv: TDBEdit
    Left = 328
    Top = 209
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule3.DataSource1
    TabOrder = 2
  end
  object Button1: TButton
    Left = 240
    Top = 236
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 374
    Top = 236
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 4
    OnClick = Button2Click
  end
  object tr: TDBLookupComboBox
    Left = 328
    Top = 182
    Width = 121
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule3.DataSource1
    ListField = 'id_treinador'
    ListSource = DataModule3.DataSource1
    TabOrder = 5
  end
end
